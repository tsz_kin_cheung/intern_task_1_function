import React, { cloneElement } from 'react';
import { useContext,useState, createContext, useReducer, Dispatch } from "react";
import './index.css';

import {taskContext} from './contexts/Task_context'; 


const getlocations= (i)=> {
  
  const rowMap = [
      [1, 1], [1, 2], [1, 3],
      [2,2], [2, 2], [2, 3],
      [3, 1], [3,2], [3, 3],
  ]
  return rowMap[i];
}

const Square=(props)=> {
  // console.log(props.winner_location)
  // console.log(props.currentgender_number)
const winner_location=props.winner_location;
const currentgender_number=props.currentgender_number;
  // console.log(winner_location.find(e => e ===currentgender_number)!=undefined)
 const current_gender_square_winner= winner_location.find(e => e ===currentgender_number)!=undefined
 console.log(current_gender_square_winner)
if(current_gender_square_winner)
  return (
    <button className="square lightblue" onClick={props.onClick}>
      {props.value}
    </button>
  );
  else
  {
    return( <button className="square white" onClick={props.onClick}>
    {props.value}
  </button> )
  }
}

const Board =(props)=> {
 const isWinnerSquare=(i)=> {
    if(props.winner && props.winner.line.findIndex(el => el === i) !== -1) {
        return true;
    }
    return null;
}

  const renderSquare=(i)=> {
    // console.log(i)
    // console.log(props)
    return (
      <Square
        value={props.squares[i]}
        winner_location={props.winner_location}
        onClick={() => props.onClick(i)}
        winner={ isWinnerSquare(i)}
        key={i}
        currentgender_number={i}
      />
    );
  }
    const col=[0,1,2]
    const row=[0,1,2]
    return (
<>
{row.map((row, index) => {
return(
  <div className="board-row" key={row}>
    {
 col.map((col, index) => {
  return(<>{renderSquare(row*3+col)}</>)
  })
    }
</div>
)})}
  </>
      // <div>
      //   <div className="board-row">
      //     {this.renderSquare(0)}
      //     {this.renderSquare(1)}
      //     {this.renderSquare(2)}
      //   </div>
      //   <div className="board-row">
      //     {this.renderSquare(3)}
      //     {this.renderSquare(4)}
      //     {this.renderSquare(5)}
      //   </div>
      //   <div className="board-row">
      //     {this.renderSquare(6)}
      //     {this.renderSquare(7)}
      //     {this.renderSquare(8)}
      //   </div>
      // </div>
    );
}

export  default ()=> {
  const data=useContext(taskContext)
// console.log(data.state)
// console.log(data.state.squares)
// console.log(data.state.xIsNext)
//   console.log(data.state.stepNumber)



const history = data.state.history;
    const current = history[data.state.stepNumber];
    const calculateWinner=(squares)=> {
      console.log(data.state.winner_location)
        const lines = [
          [0, 1, 2],
          [3, 4, 5],
          [6, 7, 8],
          [0, 3, 6],
          [1, 4, 7],
          [2, 5, 8],
          [0, 4, 8],
          [2, 4, 6],
        ];
        for (let i = 0; i < lines.length; i++) {
          const [a, b, c] = lines[i];
          if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        console.log(lines[i])
        data.state.winner_location=lines[i]
            return (squares[a]);
          }
        }
        return false;
      }
    const winner = calculateWinner(current.squares);
    // const history_copy=history
    // const history_asc=history_copy.reverse()
    // console.log(history_asc)



    const reverseArr=(input)=> {
      var ret = new Array;
      for(var i = input.length-1; i >= 0; i--) {
          ret.push(input[i]);
      }
      return ret;
  }

const history_asc=reverseArr(history)

// console.log(history_asc)
// console.log(history)
// console.log(data.state.dec)
// const historyrecordlist=data.state.dec===true?history:history_asc
    const moves =history.map((step, move) => {
      // const position=step.position===undefined?[null,null]:step.position
      // console.log(position)
      console.log(move)
      console.log(data.state.stepNumber)
      const desc = move ?
        'Go to move #' + move+
        ` ( ${history[move].position.join(',')} )`
        // ` ( ${history[move].position==undefined?" ":history[move].position[1]} )`
        : 
        'Go to game start';
      return (
        <li key={move}>
          <button onClick={
            () => {
              data.dispatch({ type: 'jumpTo',
              data:data.state,
              move:move,
              xIsNext: (move % 2) === 0});
          }
            }>
     { data.state.stepNumber===move? <strong style={{color:'red'}}>
        {desc}
        </strong>:<>{desc}</>}
              </button>
        </li>
      );
    });
    console.log(moves)
    let status;
    console.log(data.state.stepNumber)
    console.log(winner)
    if (winner) {
      status = "Winner: " + winner;
    } else {
      status = "Next player: " + (data.state.xIsNext ? "X" : "O");
    }



const handleClick=(i)=> {
  const history = data.state.history.slice(0, data.state.stepNumber + 1);
  const current = history[history.length - 1];
  const squares = current.squares.slice();
  data.state.squares=squares
  const position = getlocations(i);
  if (calculateWinner(squares) || squares[i]) {
    return;
  }
  squares[i] = data.state.xIsNext ? "X" : "O";
  data.state.xIsNext=!data.state.xIsNext;
  data.state.history= history.concat([
    {
      squares: squares,
      position:position
    }
  ])
    data.state.stepNumber= history.length
    return (data)
}

// console.log(data.state.history[data.state.stepNumber])
// console.log(data.state.stepNumber)
// console.log(data.state.history[data.state.stepNumber].squares)
// console.log(data.state.history[data.state.stepNumber].square)
// console.log(data.state)
// console.log(data.state.history[data.state.stepNumber].squares)
// console.log(data.state.winner_location)
const moves_list=data.state.dec===true?moves:reverseArr(moves)
// console.log(moves_list)
    return (
      <div className="game">
        <div className="game-board">
          <Board
               squares={data.state.history[data.state.stepNumber].squares}
               winner_location={data.state.winner_location}
               onClick={
                 ( winner===false)?(i)=>{
             // console.log(data.state.history[data.state.stepNumber].squares)
                 if(data.state.history[data.state.stepNumber].squares[i]===null)
              return( data.dispatch({ type: 'handleClick',
                   history:data.state.history.slice(0, data.state.stepNumber + 1),       
             data:handleClick(i)
            }))
              }:()=>{}
              }
          />
        </div>
        <div className="game-info">

          <div>{status}</div> 
       <button  onClick={()=>{data.dispatch({ type: 'useAscgnederlist',
            dec:false,
             });}}>
Ascending 
    </button>
    <>           </>
    <button  onClick={()=>{data.dispatch({ type: 'useAscgnederlist',
            dec:true,
             });}}>
Descending
    </button>
    { <ol>{moves_list}</ol>}
        </div>
      </div>
    );
}  





//-----------------------------------------------------------------------
//Not useful original function

 // const jumpTo=(step)=> {
  //   this.setState({
  //     stepNumber: step,
  //     xIsNext: (step % 2) === 0
  //   });
  // }

   // constructor(props) {
  //   super(props);
  //   this.state = {
  //     // history: [
  //     //   {
  //     //     squares: Array(9).fill(null)
  //     //   }
  //     // ],
  //     // stepNumber: 0,
  //     // xIsNext: true
  //   };
  // }  
  //--------------------------------------------------------------------- 





