     import React, { useContext } from "react";
    import { useState, createContext, useReducer, Dispatch } from "react";
   
  
    const initialState = {
      dec:true,
      winner_location:[null,null,null],
      data:[{
        squares:[null, null, null, null, null, null, null, null, null],
    
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true,
      }],
      squares:[null, null, null, null, null, null, null, null, null],
      count:20,
      locationList: [
        [1, 1], [1, 2], [1, 3],
        [2,2], [2, 2], [2, 3],
        [3, 1], [3,2], [3, 3],
    ],
    history: [
      {
        squares: Array(9).fill(null)
      }
    ],
    stepNumber: 0,
    xIsNext: true,
    };
  export const taskContext = createContext({ state:initialState,dispatch:()=>{}});
   
 taskContext.displayName="taskContext"
   
//  export const UPDATE_task = "UPDATE_task"

export const  MainProvider = ({children}) => {
  const [state, dispatch] = useReducer((state, action) =>{
        switch (action.type) {
            case 'handleClick':        
           return {...state,history:action.data.state.history,
                xIsNext:action.data.state.xIsNext,
                stepNumber:action.data.state.stepNumber,
              };
              case 'getWinnerLocation':   
              console.log(action.winner_location)     
              return {...state,
                 };
          case 'jumpTo':
            return{...state,stepNumber:action.move,
              xIsNext: action.xIsNext,
              winner_location:[null,null,null]
          };
          case 'useAscgnederlist':
            console.log(action.dec)
            return{...state,
     dec:action.dec
          };
          default:
            throw new Error();
        }
      }, initialState);
console.log(dispatch)

      return (
<taskContext.Provider value={{state,dispatch} }>
        
          {children}
        </ taskContext.Provider>
      );
    };
